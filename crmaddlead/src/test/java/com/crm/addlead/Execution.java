package com.crm.addlead;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Execution {

	ChromeDriver driver = Cdrive.getInstance().getDriver();
	HashMap<Integer, ExcelObject> testCases;

	public Execution(HashMap<Integer, ExcelObject> atestCases) {
		this.testCases = atestCases;
	}

	public Map<Integer, ExcelObject> executeLoginTestCase() {
		Set<Integer> key = testCases.keySet();
		for (int caseNo : key) {
			ExcelObject atestRecord = testCases.get(caseNo);
			Boolean isTrue = Feature.logIn(atestRecord, driver);
			if (isTrue)
				atestRecord.setActualResult(Feature.logOut(driver));
			atestRecord.setActualResult(isTrue);
		}
		return testCases;
	}

	public HashMap<Integer, ExcelObject> executeAddLeadTestCase() {
		Set<Integer> key = testCases.keySet();
		ExcelObject atestRecord;
		for (int caseNo : key) {
			atestRecord = testCases.get(caseNo);
			Boolean isTrue = Feature.logIn(atestRecord, driver);
			boolean addlead, logout;
			if (isTrue) {
				addlead = Feature.addLead(atestRecord, driver);
				System.out.println("addLead  " + addlead);
				logout = Feature.logOut( driver);
				System.out.println("logOut  " + logout);
				atestRecord.setActualResult(addlead & logout);
			} else
				atestRecord.setActualResult(isTrue);
			String s = (atestRecord.getActualResult()==atestRecord.getExpectedResult()) ? "PASS" : "FAIL";
            atestRecord.setOverAllResult(s);
		}

		return testCases;

	}

}
