package stepDefenitions;

import java.time.Duration;
import java.util.HashMap;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.crm.addlead.Cdrive;
import com.crm.addlead.Element;
import com.crm.addlead.ExcelHandle;
import com.crm.addlead.ExcelObject;
import com.crm.addlead.Feature;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class addLeadStepDef {
	ChromeDriver driver = Cdrive.getInstance().getDriver();
	HashMap<Integer, ExcelObject> testCases;
	ExcelObject atestRecord;
	boolean isTrue;

	@Before
	public void getData() {
		testCases = ExcelHandle.getInstanse(33, 4).readExcel("D:\\SeleniumExcel\\Login_test_case_auto.xls");
	}

	@Given("user is on login page")
	public void user_is_on_login_page() {
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		atestRecord = testCases.get(1);
		System.out.println(atestRecord);
		isTrue = Feature.logIn(atestRecord, driver);
		driver.findElement(Element.lead).click();
	}

	@When("user try to add the Lead in Lead List")
	public void user_try_to_add_the_Lead_in_Lead_List() {
		if (isTrue) {
			Set<Integer> key = testCases.keySet();
			for (int caseNo : key) {
				atestRecord = testCases.get(caseNo);
				boolean addlead = Feature.addLead(atestRecord, driver);
				System.out.println("addLead  " + addlead);
				atestRecord.setActualResult(addlead);
				
			}
		} else
			atestRecord.setActualResult(isTrue);
		String s = (atestRecord.getActualResult() == atestRecord.getExpectedResult()) ? "PASS" : "FAIL";
		atestRecord.setOverAllResult(s);
	}

	@When("user loged Out")
	public void user_loged_Out() {
		Feature.logOut(driver);
	}

	@After
	public void setData() {
		testCases = ExcelHandle.getInstanse(33, 4).readExcel("D:\\SeleniumExcel\\Login_test_case_auto.xls");
	}

}
