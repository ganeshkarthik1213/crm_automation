package com.crm.addlead;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.idealized.Javascript;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Feature {

	private static String originalSpam = "Invalid Details. Please check the UserName-Password combination.";

	public static Boolean logIn(ExcelObject atestRecord, ChromeDriver driver) {
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		WebDriverWait w = new WebDriverWait(driver, 4);
		String url = "http://50.112.4.157:8011/business#/login";
		driver.manage().window().maximize();
		Map<Integer, String> input = atestRecord.getInput();
		String username = input.get(1);
		String password = input.get(2);
		driver.get(url);
		driver.findElement(Element.userName).sendKeys(username);
		driver.findElement(Element.password).sendKeys(password);

		driver.findElement(Element.login).click();
//		System.out.println("is clickable"+ w.until(ExpectedConditions.elementToBeClickable(Element.login)));
		try {
			Thread.sleep(5000);
			if (driver.getCurrentUrl().toLowerCase().contains(Element.isInLoginPage))
				return !(checkTheText(driver.findElement(Element.spamMsg).getText(), originalSpam));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		w.until(ExpectedConditions.elementToBeClickable(driver.findElement(Element.profileName)));
		return checkTheText(driver.findElement(Element.profileName).getText(), username);

	}

	private static Boolean checkTheText(String txt, String originalText) {
		System.out.println("text to validate " + txt);
		System.out.println(" original text " + originalText + "  " + txt.trim().equalsIgnoreCase(originalText.trim()));
		if (txt.trim().equalsIgnoreCase(originalText.trim()))
			return true;
		return false;
	}

	public static boolean logOut(ChromeDriver driver) {
		try {
			Thread.sleep(3000);
			Actions a = new Actions(driver);
			WebDriverWait w = new WebDriverWait(driver, 5);
			w.until(ExpectedConditions.visibilityOf(driver.findElement(Element.profileName)));
			a.moveToElement(driver.findElement(Element.profileImage)).build().perform();
			System.out.println("user going to " + driver.findElement(Element.logOut).getText());
			driver.findElement(Element.logOut).click();
			Thread.sleep(2000);
			System.out.println("current url  " + driver.getCurrentUrl());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Cdrive.getInstance().clearCookies();
		return driver.getCurrentUrl().contains(Element.isInLoginPage);

	}

	public static boolean addLead(ExcelObject atestRecord, ChromeDriver driver) {
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		
		Map<Integer, String> input = atestRecord.getInput();
		String Salutation = input.get(3);
		String firstName = input.get(4);
		String title = input.get(5);
		String company = input.get(6);
		String webSite = input.get(7);
		String industry = input.get(8);
		String primaryEmail = input.get(9);
		String secondaryEmail = input.get(10);
		String emailstatus = input.get(11);
		String leadOwner = input.get(12);
		String leadSource = input.get(13);
		String street = input.get(14);
		String state = input.get(15);
		String country = input.get(16);
		String countryy = input.get(17);
		String description = input.get(18);
		String leadStatus = input.get(33);
		String zipCode = input.get(32);
		String city = input.get(31);
		String annualRevenue = input.get(30);
		String linkedInStatus = input.get(29);
		String linkedIn = input.get(28);
		String twitter = input.get(27);
		String skypeId = input.get(26);
		String noOfEmployees = input.get(25);
		String fax = input.get(24);
		String mobileCountry = input.get(22);
		String mobileNumber = input.get(23);
		String lastName = input.get(21);
		String phoneCountry = input.get(19);
		String phoneNumber = input.get(20);

		WebDriverWait w = new WebDriverWait(driver, 8);
		w.until(ExpectedConditions.visibilityOf(driver.findElement(Element.viewTopLead)));
		driver.findElement(Element.addLead).click();
		w.until(ExpectedConditions.visibilityOf(driver.findElement(Element.imageUploadIcon)));
		selectSalutation(nullCheck(Salutation), driver);
		driver.findElement(Element.firstname).sendKeys(nullCheck(firstName));
		driver.findElement(Element.title).sendKeys(nullCheck(title));
		driver.findElement(Element.companyName).sendKeys(nullCheck(company));
		driver.findElement(Element.webSite).sendKeys(nullCheck(webSite));

		selectDropDown(driver.findElement(Element.industry), nullCheck(industry));
		driver.findElement(Element.primaryEmail).sendKeys(nullCheck(primaryEmail));
		driver.findElement(Element.secondaryEmail).sendKeys(nullCheck(secondaryEmail));

		selectDropDown(driver.findElement(Element.emailstatus), nullCheck(emailstatus));
		driver.findElement(Element.leadOwner).sendKeys(nullCheck(leadOwner));

		selectDropDown(driver.findElement(Element.leadSource), nullCheck(leadSource));
		driver.findElement(Element.street).sendKeys(nullCheck(street));
		driver.findElement(Element.state).sendKeys(nullCheck(state));

		selectDropDown(driver.findElement(Element.country), nullCheck(country));
		driver.findElement(Element.countryy).sendKeys(nullCheck(countryy));
		driver.findElement(Element.description).click();
		driver.findElement(Element.description).sendKeys(nullCheck(description));

		selectDropDown(driver.findElement(Element.leadStatus), nullCheck(leadStatus));
		driver.findElement(Element.zipCode).sendKeys(nullCheck(zipCode));
		driver.findElement(Element.city).sendKeys(nullCheck(city));
		driver.findElement(Element.annualRevenue).sendKeys(nullCheck(annualRevenue));

		selectDropDown(driver.findElement(Element.linkedInStatus), nullCheck(linkedInStatus));
		driver.findElement(Element.linkedIn).sendKeys(nullCheck(linkedIn));
		driver.findElement(Element.twitter).sendKeys(nullCheck(twitter));
		driver.findElement(Element.skypeId).sendKeys(nullCheck(skypeId));
		driver.findElement(Element.noOfEmployees).sendKeys(nullCheck(noOfEmployees));
		driver.findElement(Element.fax).sendKeys(nullCheck(fax));
		driver.findElement(Element.mobileCountry).click();
		w.until(ExpectedConditions.visibilityOf(driver.findElement(Element.mobileCountryList)));

		dropDown(driver.findElements(Element.mobileCountryList), nullCheck(mobileCountry));
		driver.findElement(Element.mobileNumber).sendKeys(nullCheck(mobileNumber));
		driver.findElement(Element.lastName).sendKeys(nullCheck(lastName));
		driver.findElement(Element.phoneCountry).click();
		w.until(ExpectedConditions.visibilityOf(driver.findElement(Element.phoneCountryList)));
		dropDown(driver.findElements(Element.phoneCountryList), nullCheck(phoneCountry));
		driver.findElement(Element.phoneNumber).sendKeys(nullCheck(phoneNumber));

		List<WebElement> listOfMandatoryElement = new ArrayList<WebElement>();
		listOfMandatoryElement.add(driver.findElement(Element.firstname));
//		listOfMandatoryElement.add(driver.findElement(Element.state));
//		listOfMandatoryElement.add(driver.findElement(Element.city));
		boolean isFilled = checkForMandatoryField(listOfMandatoryElement);
		driver.findElement(Element.addLeadAfterInput).click();
		if (isFilled) {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scroll(500,0)");
			driver.findElement(Element.BackButton).click();
			return !checkTheText(driver.findElement(Element.errorForaddLead).getText(), Element.errorMsgForAddLead);
		}
		w.until(ExpectedConditions.visibilityOf(driver.findElement(Element.successForAddLead)));
		String successMsg = driver.findElement(Element.successForAddLead).getText();
		System.out.println("success msg " + successMsg);
		w.until(ExpectedConditions.visibilityOf(driver.findElement(Element.viewTopLead)));
		System.out.println("lead Name after add " + driver.findElement(Element.viewTopLead).getText());
		return checkTheText(successMsg, Element.successMsgForAddLead)
				& checkTheText(driver.findElement(Element.viewTopLead).getText(), firstName);
	}

	private static boolean checkForMandatoryField(List<WebElement> listOfMandatoryElement) {

		for (WebElement e : listOfMandatoryElement) {
			System.out.println(
					" Mandatoty Field " + e.getAttribute("value") + "  isBlank " + (e.getAttribute("value").isBlank()));
			if (e.getAttribute("value").isBlank())
				return true;
		}
		return false;

	}

	private static void dropDown(List<WebElement> findElements, String StringToMatch) {
		List<WebElement> list = findElements.stream()
				.filter(w -> w.getText().toLowerCase().contains(StringToMatch.toLowerCase()))
				.collect(Collectors.toList());
		System.out.println(list.size() + " size");
		System.out.println("List of country " + list);
		list.get(0).click();
	}

	private static void selectDropDown(WebElement webElement, String stringToMatch) {

		Select select = new Select(webElement);
		List<WebElement> list = select.getOptions();
		for (WebElement w : list) {
			if (w.getText().equalsIgnoreCase(stringToMatch)) {
				w.click();
				break;
			}
		}
	}

	private static void selectSalutation(String salutation, ChromeDriver driver) {
		switch (salutation.toLowerCase()) {
		case "mr":
			driver.findElement(Element.mr).click();
			break;
		case "ms":
			driver.findElement(Element.ms).click();
			break;
		case "mrs":
			driver.findElement(Element.mrs).click();
			break;
		default:

		}
	}

	private static String nullCheck(String value) {
		if (value == null)
			return "";
		return value;
	}

}
