package com.crm.addlead;

import org.openqa.selenium.By;

public class Element {
	
	public static By userName = By.id("username");
	public static By password = By.id("password");
    public static By login = By.id("businessLogin");
    public static By spamMsg = By.xpath("//span[@class='ng-binding']");
    public static By profileImage = By.id("profileImage");
    public static By profileName = By.id("userName");
    public static By logOut = By.xpath("//a[contains(@class,'logoutIco')]//span[contains(@class,'allign-Self-Center')]");
    public static String isInLoginPage = "login";
    public static By lead = By.xpath("//a[@ui-sref='leadDetails']");
    public static By addLead = By.xpath("//button[@title='Add Lead']");
    public static By mr = By.id("radio0");
    public static By ms = By.id("radio1");
    public static By mrs = By.id("radio2");
    public static By firstname = By.xpath("((//div[@class='contInfoTab floatL'])[2]//input[@name='value2'])[1]"); 
    public static By title = By.xpath("((//div[@class='contInfoTab floatL'])[2]//input[@name='value3'])[1]");
    public static By companyName = By.xpath("((//div[@class='contInfoTab floatL'])[2]//input[@name='value4'])[1]");
    public static By webSite = By.xpath("((//div[@class='contInfoTab floatL'])[2]//input[@name='value5'])[1]");
    public static By industry = By.id("select5");
    public static By primaryEmail = By.xpath("((//div[@class='contInfoTab floatL'])[2]//input[@name='value7'])[1]");
    public static By secondaryEmail = By.xpath("((//div[@class='contInfoTab floatL'])[2]//input[@name='value8'])[1]");
    public static By emailstatus = By.id("select8");
    public static By leadOwner = By.xpath("//div[contains(@data-ng-show,'Lead Owner') and @aria-hidden='false']//input[@id='ex1_value']");
    public static By leadSource = By.id("select10");
    public static By street = By.xpath("((//div[@class='contInfoTab floatL'])[3]//input[@name='value1'])[1]");
    public static By state = By.xpath("((//div[@class='contInfoTab floatL'])[3]//input[@name='value2'])[1]");
    public static By country = By.xpath("(//div[@class='contInfoTab floatL'])[3]//select[@id='select2']");
    public static By countryy = By.xpath("((//div[@class='contInfoTab floatL'])[3]//input[@name='value4'])[1]");
    public static By description = By.xpath("(//div[@class='contInfoTab floatL'])[4]//textarea[@ng-model='data.value']");
    public static By leadStatus = By.xpath("(//div[@class='contInfoTab floatR'])[4]//select[@id='select0']");
    public static By zipCode = By.xpath("((//div[@class='contInfoTab floatR'])[3]//input[@name='value2'])[1]");
    public static By city = By.xpath("((//div[@class='contInfoTab floatR'])[3]//input[@name='value1'])[1]");
    public static By annualRevenue = By.xpath("((//div[@class='contInfoTab floatR'])[2]//input[@name='value10'])[2]");
    public static By linkedInStatus = By.xpath("(//div[@class='contInfoTab floatR'])[2]//select[@id='select8']");
    public static By linkedIn = By.xpath("((//div[@class='contInfoTab floatR'])[2]//input[@name='value8'])[1]");
    public static By twitter = By.xpath("((//div[@class='contInfoTab floatR'])[2]//input[@name='value7'])[1]"); 
    public static By skypeId = By.xpath("((//div[@class='contInfoTab floatR'])[2]//input[@name='value6'])[1]");
    public static By noOfEmployees = By.xpath("((//div[@class='contInfoTab floatR'])[2]//input[@name='value5'])[2]");
    public static By fax = By.xpath("((//div[@class='contInfoTab floatR'])[2]//input[@name='value4'])[1]");
    public static By mobileCountryList = By.xpath("(//ul[@aria-label='List of countries'])[2]//span[@class='iti__country-name']");
    public static By ismobileCountryshown = By.xpath("((//ul[@aria-label='List of countries'])[2]//span[@class='iti__country-name'])[1]");
    public static By mobileCountry = By.xpath("((//div[@class='contInfoTab floatR'])[2]//div[@class='iti__selected-flag'])[2]");
    public static By mobileNumber = By.id("phone223");
    public static By lastName = By.xpath("(((//div[@class='contInfoTab floatR'])[2])//input[@name='value2'])[1]");
    public static By phoneCountryList = By.xpath("(//ul[@aria-label='List of countries'])[1]//span[@class='iti__country-name']");
    public static By isphoneCountryshown = By.xpath("((//ul[@aria-label='List of countries'])[1]//span[@class='iti__country-name'])[1]");
    public static By phoneCountry = By.xpath("((//div[@class='contInfoTab floatR'])[2]//div[@class='iti__selected-flag'])[1]");
    public static By phoneNumber = By.id("phone221");
    public static By addLeadAfterInput = By.id("saveCustomer");
    public static By viewTopLead = By.xpath("((//tbody/tr[@title='View Lead'][1])[1]//td[@class='ng-scope'])[1]"); 
    public static By imageUploadIcon = By.id("imageUploadIcon");
    public static By errorForaddLead = By.xpath("//div[@class='ng-binding ng-scope']");
    public static By successForAddLead = By.xpath("//div[@class='toast ng-scope toast-success']//div[@class='ng-binding ng-scope']");
    public static String errorMsgForAddLead = "First Name Column is Mandatory";
    public static String successMsgForAddLead = "Customer Details Saved Successfully.. ";
    public static By BackButton = By.xpath("//button[@title='Back']");
    
    
}
