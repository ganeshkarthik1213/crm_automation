package com.crm.addlead;

import org.openqa.selenium.chrome.ChromeDriver;

public class Cdrive {
	private Cdrive() {}
	private static Cdrive instsnce = null;
	private  ChromeDriver driver = null;
	public static Cdrive getInstance()
	{
		if ( instsnce== null)
			instsnce = new Cdrive();
		
		return instsnce;
	}
	public  ChromeDriver getDriver()
	{
		if(driver==null) {
			System.setProperty("webdriver.chrome.driver","C:\\selenium\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		return driver;
	}
	public void clearCookies()
	{
		driver.manage().deleteAllCookies();
	}

}
