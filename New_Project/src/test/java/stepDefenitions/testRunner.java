package stepDefenitions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/Features/crm_addlead.feature",glue= {"stepDefenitions"})
public class testRunner {

}
