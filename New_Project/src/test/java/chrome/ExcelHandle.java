package chrome;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExcelHandle {
	private HashMap<Integer, ExcelObject> testCase = new HashMap<Integer, ExcelObject>();
	private static ExcelHandle EH = null;
	private int inputColumn;
	private int inputRows;

	public static ExcelHandle getInstanse(int ainputColumn, int ainputRows) {
		if (EH == null)
			EH = new ExcelHandle(ainputColumn, ainputRows);
		return EH;
	}

	private ExcelHandle(int ainputColumn, int ainputRows) {

		this.inputColumn = ainputColumn;
		this.inputRows = ainputRows;
	}

	public HashMap<Integer, ExcelObject> readExcel(String filePath) {
		try {
			FileInputStream fis = new FileInputStream(new File(filePath));
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			HSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> itr = sheet.rowIterator();
			getRows(itr);
			fis.close();
		} catch (Exception e) {
			System.out.println("exception in excel" + e);
			e.printStackTrace();
		}
		System.out.println(testCase);
		return testCase;

	}

	private void getRows(Iterator<Row> itr) {
		Row row =  itr.next();
		while (itr.hasNext()) {
			row = itr.next();
			System.out.println("actual Row number " + row.getRowNum() + "input rows  " + inputRows);
			if (row.getRowNum() > inputRows)
				break;
			setValues(row);
		}
	}

	private void setValues(Row row) {
		Iterator<Cell> cellIterator = row.cellIterator();
		Cell cell = null;
		HashMap<Integer, String> kv = new HashMap<Integer, String>();
		ExcelObject exobj = new ExcelObject();
		while (cellIterator.hasNext()) {
			
			cell = cellIterator.next();

			switch (cell.getCellType()) {

			case Cell.CELL_TYPE_BLANK:
				break;

			case Cell.CELL_TYPE_NUMERIC:
				if (cell.getColumnIndex() == 0)
					exobj.setSno((int) cell.getNumericCellValue());
				break;

			case (Cell.CELL_TYPE_STRING):

				while (cell.getColumnIndex() <= inputColumn) {
					kv.put(cell.getColumnIndex(), cell.getStringCellValue());
					if (cellIterator.hasNext())
						cell = cellIterator.next();
				}
				if (cell.getColumnIndex() == inputColumn + 1)
					exobj.setExpectedResult(cell.getBooleanCellValue());
				break;

			}
		}
		exobj.setInput(kv);
		testCase.put(row.getRowNum(), exobj);
	}

	public void writeExcel(Map<Integer, ExcelObject> outCase,String filePath) {
		try {
			FileInputStream fis = new FileInputStream(new File(filePath));
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			HSSFSheet sheet = workbook.getSheet("Login_test_case_auto");
			Iterator<Row> itr = sheet.iterator();
			itr.next();
			while (itr.hasNext()) {
				Row row = itr.next();
				Cell cell = row.createCell(inputColumn + 2);
				cell.setCellValue(outCase.get(cell.getRowIndex()).getActualResult());
				Cell cell1 = row.createCell(inputColumn + 3);
				cell1.setCellValue(outCase.get(cell1.getRowIndex()).getOverAllResult());
			}
			fis.close();
			FileOutputStream fos = new FileOutputStream(new File(filePath));
			workbook.write(fos);
			fos.close();
			System.out.println(" written successfully on disk.");

		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

//	public static void main(String[] args) {
//HashMap<Integer, ExcelObject> map = new ExcelHandle(33, 3).readExcel();
//		System.out.println(map);
//		new ExcelHandle(33,3).writeExcel(map);
//
//	}

}