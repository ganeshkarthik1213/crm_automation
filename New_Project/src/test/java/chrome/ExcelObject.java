package chrome;

import java.util.Map;

public class ExcelObject {

	private boolean expectedResult ;
	private boolean actualResult ;
	private String overAllResult ;
	private int sno ;
	private Map<Integer, String> input ;
	
	public Map<Integer, String> getInput() {
		return input;
	}
	public void setInput(Map<Integer, String> kv) {
		this.input = kv;
	}
	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public boolean getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(boolean b) {
		this.expectedResult = b;
	}
	public boolean getActualResult() {
		return actualResult;
	}
	public void setActualResult(boolean b) {
		this.actualResult = b;
	}
	public String getOverAllResult() {
		return overAllResult;
	}
	public void setOverAllResult(String s) {
		this.overAllResult = s;
	}
	
	
	@Override
	public String toString() {
		return "ExcelObject [expectedResult=" + expectedResult + ", actualResult=" + actualResult + ", overAllResult="
				+ overAllResult + ", sno=" + sno + ", input=" + input + "]";
	}
	
	
	

}

